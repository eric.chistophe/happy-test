$(function() {
  var $form = $(".upload-form");
  var $label = $("#file_field_label");
  var $file_input = $("#attachement");
  var uploaded_file = null;

  function displayFileName(files) {
    $label.text(files[0].name);
    uploaded_file = new FormData();
    uploaded_file.append("attachement", files[0]);
  }

  $form.on( 'drag dragstart dragend dragover dragenter dragleave drop', function( e ){
		e.preventDefault();
		e.stopPropagation();
	});

  $form.on( 'drop', function( e ){
    droppedFiles = e.originalEvent.dataTransfer.files;
    $file_input.attr('name', droppedFiles[0]);
    displayFileName( droppedFiles );
	});

  $file_input.on('change', function(e){
    displayFileName(e.target.files);
	});

  $form.on("submit", function (e){
    e.preventDefault();

    if (uploaded_file == null) {
      return false;
    }

    $(".alert").css('display', 'none');
    $(".alert.uploading").css('display', 'block');

    $.ajax({
      type: 'POST',
      url: "/images/",
      data: uploaded_file,
      dataType: 'json',
      processData: false,
      contentType: false
    })
    .done(function(data) {
      $label.text("Click to Upload");
      $('.submit-btn').removeAttr('disabled');
      $(".alert").css('display', 'none');
      $(".alert.success").css('display', 'block');
      $(".images-list").prepend(data.html_content);
      setTimeout(function(){ $(".alert").css('display', 'none'); }, 1500);
    })
    .fail(function(data) {
      var errors = data.responseJSON.errors;
      var errorsText = "";

      for (i = 0; i < errors.length; i++) {
        errorsText += "<li>"+errors[i]+"</li>";
      }

      $(".alert").css('display', 'none');
      $(".alert.error ul").html(errorsText);
      $(".alert.error").css('display', 'block');
      $('.submit-btn').removeAttr('disabled');
    });
  });

  $('.images-list').on('click', '.delete-btn', function(e){
    var $elt = $(this);
    var $parent_container = $elt.parents('.image-card');
    $.ajax({
      type: 'DELETE',
      url: "/images/"+$elt.data('attribute')
    })
    .done(function(data) {
      $parent_container.hide('slow', function(){ $parent_container.remove(); });;
    })
    .fail(function(data) {
      console.log("error");
    });
  });

  $('.images-list').on('click', '.image-link', function(e){
    $(this).select();
    document.execCommand("copy");
    $('#tooltip').css('display', 'block');
    setTimeout(function(){ $('#tooltip').css('display', 'none'); }, 1500);
  });

});
