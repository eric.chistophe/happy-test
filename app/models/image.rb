class Image < ApplicationRecord
  mount_uploader :attachement, AttachementUploader
  default_scope { order(created_at: :desc) }

  validates :attachement, :ip_address, presence: true

  validate :check_image_dimensions, on: :create

  attr_accessor :width, :height, :size

  private

  def check_image_dimensions
    if !width.nil? && width > 1000
      errors.add :attachement, "width is more than 1000px."
    end
    if !height.nil? && height > 1000
      errors.add :attachement, "height is more than 1000px."
    end
    if !size.nil? && size > 250000
      errors.add :attachement, "size is more than 250kb."
    end
  end
end
